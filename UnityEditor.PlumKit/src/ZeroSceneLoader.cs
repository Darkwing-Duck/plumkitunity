﻿#region Using Statements
using UnityEngine;
using UnityEditor;
using System.Collections;
#endregion

namespace PlumKit
{
    [InitializeOnLoad]
    public class ZeroSceneLoader
    {
        private static bool _isInPlayMode = false;
        
        static ZeroSceneLoader()
        {
            EditorApplication.playmodeStateChanged += OnPlaymodeStateChanged;
        }
        
        private static void OnPlaymodeStateChanged()
        {
            if (_isInPlayMode == EditorApplication.isPlaying)
            {
                return;
            }
            
            _isInPlayMode = EditorApplication.isPlaying;
            
            if (_isInPlayMode)
            {
                OnEditModeExit();
                OnPlayModeEnter();
            }
            else
            {
                OnPlayModeExit();
                OnEditModeEnter();
            }
        }
        
        private static void OnPlayModeEnter()
        {
            if (Application.loadedLevel == 0)
            {
                return;
            }
            
            Application.LoadLevel(0);
        }
        
        private static void OnPlayModeExit()
        {
        }
        
        private static void OnEditModeEnter()
        {
        }
        
        private static void OnEditModeExit()
        {
        }
    }
}