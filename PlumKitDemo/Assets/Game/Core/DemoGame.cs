﻿using UnityEngine;
using System.Collections;
using PlumKit;

namespace Demo
{
    public class DemoGame : PKGame
    {
        public DemoGame(PKGameControllerComponent gameController) : base(gameController)
        {
        }
        
        protected override IPKLogger CreateLogger()
        {
            IPKLogger logger = new PKConsoleLogger();
            logger.RegisterGroup(DemoLoggerGroups.DEMO, "DEMO");
            return logger;
        }
        
        protected override void OnStart()
        {
            base.OnStart();
            Logger.Log("Test Log");
            Logger.LogInfo("Test Info");
            Logger.LogDebug("Test Debug");
            Logger.LogWarning("Test Warning");
            Logger.LogError("Test Error");
            
            SlashCommand command = new SlashCommand("addMoney", "Adds Money.");
            command.Callback = AddMoney;
            Logger.RegisterCommand(command);
        }
        
        private void AddMoney(SlashCommand command)
        {
            Logger.LogInfo("<- The " + command.Args[0] + " was successful Added!");
        }

        protected override IPKScene CreateLoadingScene()
        {
            return new LoadingScene(this, _sceneManager);
        }
        
        protected override void RegisterScenes()
        {
            base.RegisterScenes();
            RegisterScene(new GameplayScene(this, _sceneManager));
        }
    }
}