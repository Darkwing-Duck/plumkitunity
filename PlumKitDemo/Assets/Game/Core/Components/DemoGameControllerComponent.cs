﻿#region Using Statements
using UnityEngine;
using System.Collections;
using PlumKit;
using System;
#endregion

namespace Demo
{
    public class DemoGameControllerComponent : PKGameControllerComponent
    {
        protected override Type GetGameType()
        {
            return typeof(DemoGame);
        }
    }
}