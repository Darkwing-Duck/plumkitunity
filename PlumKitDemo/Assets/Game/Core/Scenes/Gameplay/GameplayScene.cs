﻿#region Using Statements
using UnityEngine;
using System.Collections;
using PlumKit;
#endregion

namespace Demo
{
    public class GameplayScene : PKScene
    {
        public const string ALIAS = "GameplayScene";
        
        public GameplayScene(IPKGame game, IPKSceneManager sceneManager) : base(game, sceneManager)
        {
            
        }
        
        public override string Alias
        {
            get
            {
                return ALIAS;
            }
        }
    }
}