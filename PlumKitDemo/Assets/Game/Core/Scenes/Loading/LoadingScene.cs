﻿#region Using Statements
using UnityEngine;
using System.Collections;
using PlumKit;
#endregion

namespace Demo
{
    public class LoadingScene : PKLoadingScene
    {
        public LoadingScene(IPKGame game, IPKSceneManager sceneManager) : base(game, sceneManager)
        {
        }
        
        protected override void OnActivate()
        {
            base.OnActivate();
//            _game.GoToScene(GameplayScene.ALIAS);
        }
        
        public override string Alias
        {
            get
            {
                return "DemoLoadingScene";
            }
        }
    }
}