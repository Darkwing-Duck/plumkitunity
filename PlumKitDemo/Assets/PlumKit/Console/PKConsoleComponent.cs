﻿#region Using Statements
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
#endregion

namespace PlumKit
{
    public class PKConsoleComponent : PKComponent 
    {
        #region Constants
        private const string SCROLL_RECT_PATH = "ScrollRect";
        private const string MESSAGE_CONTAINER_PATH = "ScrollRect/Messages";
        #endregion
        
        #region Prefabs
        public GameObject MessagePrefab;
        #endregion
        
        #region Colors
        public Color LogColor;
        public Color InfoColor;
        public Color DebugColor;
        public Color WarningColor;
        public Color ErrorColor;
        public Color CommandColor;
        #endregion
        
        #region Members
        private Transform _messageContainer;
        private ScrollRect _scrollRect;
        #endregion
        
        #region MonoBehaviour Handlers
        protected override void Awake () 
        {
            base.Awake();
            _messageContainer = transform.FindChild(MESSAGE_CONTAINER_PATH);
            _scrollRect = transform.FindChild(SCROLL_RECT_PATH).GetComponent<ScrollRect>();
        }
        #endregion
        
        #region PKComponent Handlers
        protected override void ClearReferences()
        {
            base.ClearReferences();
            
            _messageContainer = null;
            _scrollRect = null;
        }
        #endregion
        
        public  void LogMessage(string message, LogLevel level)
        {
            switch (level)
            {
                case LogLevel.LOG:
                {
                    Log(message);
                    break;
                }
                case LogLevel.INFO:
                {
                    LogInfo(message);
                    break;
                }
                case LogLevel.DEBUG:
                {
                    LogDebug(message);
                    break;
                }
                case LogLevel.WARNING:
                {
                    LogWarning(message);
                    break;
                }
                case LogLevel.ERROR:
                {
                    LogError(message);
                    break;
                }
                case LogLevel.COMMAND:
                {
                    LogCommand(message);
                    break;
                }
            }
        }
        
        private void Log(string message)
        {
            AddMessage(message, LogColor);
        }
        
        private void LogInfo(string message)
        {
            AddMessage(message, InfoColor);
        }
        
        private void LogDebug(string message)
        {
            AddMessage(message, DebugColor);
        }
        
        private void LogWarning(string message)
        {
            AddMessage(message, WarningColor);
        }
        
        private void LogError(string message)
        {
            AddMessage(message, ErrorColor);
        }
        
        private void LogCommand(string message)
        {
            AddMessage(message, CommandColor);
        }
        
        protected void AddMessage(string message, Color color)
        {
            GameObject messageInstance = (GameObject)Instantiate(MessagePrefab);
            Text textComponent = messageInstance.GetComponent<Text>();
            
            messageInstance.transform.SetParent(_messageContainer);
            textComponent.color = color;
            textComponent.text = message;
            
            _scrollRect.verticalNormalizedPosition = 0;
        }
    }
}