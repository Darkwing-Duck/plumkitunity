﻿#region Using Statements
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
#endregion

namespace PlumKit
{
    public class PKCommandLineComponent : PKComponent 
    {
        public int MinCommandLength = 3;
        public InputField InputField;
        public Button SendButton;
        public PKConsoleComponent Console;
        
        protected override void Awake()
        {
            base.Awake();
        }
        
        protected override void Start()
        {
            base.Start();
            ResetInput();
            OnInputValueChanged();
        }
        
        protected void ResetInput()
        {
            InputField.text = "";
        }
        
        protected bool IsCommandValid()
        {
            if (!IsCommandLengthValid())
            {
                return false;
            }
        
            return true;
        }
        
        protected bool IsCommandLengthValid()
        {
            if (InputField.text.Length < MinCommandLength)
            {
                return false;
            }
            
            return true;
        }

        public void OnInputValueChanged()
        {
            SendButton.enabled = IsCommandValid(); 
        }
        
        public void SendCommand()
        {
            if (!IsCommandLengthValid())
            {
                return;
            }

            Game.Logger.ExecuteCommand(InputField.text); 
            ResetInput();
        }
        
        protected override void WillBeDestroyed()
        {
            base.WillBeDestroyed();
        }
    }
}