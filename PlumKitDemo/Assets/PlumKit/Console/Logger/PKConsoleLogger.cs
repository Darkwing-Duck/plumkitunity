using System;
using UnityEngine;

namespace PlumKit
{
    public class PKConsoleLogger : PKDefaultLogger
    {
        protected PKConsoleComponent _console;
        
        public PKConsoleLogger() : base()
        {
            InitConsole();
        }
        
        protected void InitConsole()
        {
            _console = GetConsole();
            
            if (_console == null)
            {
                LogWarning("Can't find 'PKConsoleComponent' on scene!");
            }
        }
        
        protected virtual PKConsoleComponent GetConsole()
        {
            return GameObject.FindObjectOfType<PKConsoleComponent>();
        }
        
        public override void Log(object message)
        {
            base.Log(message);
            LogToConsole(message, LogLevel.LOG);
        }
        
        public override void Log(object message, Enum groupId)
        {
            base.Log(message, groupId);
            LogToConsole(GetGroupPrefix(groupId) + message, LogLevel.LOG);
        }
        
        public override void LogInfo(object message)
        {
            base.LogInfo(message);
            LogToConsole(message, LogLevel.INFO);
        }
        
        public override void LogInfo(object message, Enum groupId)
        {
            base.LogInfo(message, groupId);
            LogToConsole(GetGroupPrefix(groupId) + message, LogLevel.INFO);
        }
        
        public override void LogDebug(object message)
        {
            base.LogDebug(message);
            LogToConsole(message, LogLevel.DEBUG);
        }
        
        public override void LogDebug(object message, Enum groupId)
        {
            base.LogDebug(message, groupId);
            LogToConsole(GetGroupPrefix(groupId) + message, LogLevel.DEBUG);
        }
        
        public override void LogWarning(object message)
        {
            base.LogWarning(message);
            LogToConsole(message, LogLevel.WARNING);
        }
        
        public override void LogWarning(object message, Enum groupId)
        {
            base.LogWarning(message, groupId);
            LogToConsole(GetGroupPrefix(groupId) + message, LogLevel.WARNING);
        }
        
        public override void LogError(object message)
        {
            base.LogError(message);
            LogToConsole(message, LogLevel.ERROR);
        }
        
        public override void LogError(object message, Enum groupId)
        {
            base.LogError(message, groupId);
            LogToConsole(GetGroupPrefix(groupId) + message, LogLevel.ERROR);
        }
        
        public override void LogCommand(SlashCommand command)
        {
            base.LogCommand(command);
            LogToConsole(command.ToString(), LogLevel.COMMAND);
        }
        
        protected void LogToConsole(object message, LogLevel level)
        {
            if (_console == null)
            {
                return;
            }
            
            _console.LogMessage(message.ToString(), level);
        }
    }
}