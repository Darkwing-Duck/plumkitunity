using UnityEngine;
using System.Collections;
using System;

namespace PlumKit
{
    public class PKComponent : MonoBehaviour
    {
        public IPKGame Game { get; private set; }
    
        protected virtual void Awake()
        {
            PKGameControllerComponent gameController = FindObjectOfType<PKGameControllerComponent>();
        
            if (gameController == null)
            {
                throw new Exception("Can't find game controller ('PKGameControllerComponent')!");
            }
    
            Game = gameController.Game;
        }
        
        protected virtual void Start()
        {
        }
    
        protected virtual void WillBeDestroyed()
        {
            //
        }
    
        protected virtual void OnDestroy()
        {
            WillBeDestroyed();
            ClearReferences();
        }
    
        protected virtual void ClearReferences()
        {
            Game = null;
        }
    }
}