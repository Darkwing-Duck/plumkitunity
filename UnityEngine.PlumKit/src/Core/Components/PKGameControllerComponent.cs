﻿using UnityEngine;
using System.Collections;
using System;

namespace PlumKit
{
    abstract public class PKGameControllerComponent : MonoBehaviour
    {
        public IPKGame Game { get; private set; }
    
        protected virtual void Awake()
        {
            DontDestroyOnLoad(gameObject);

            Type type = GetGameType();
            object[] args = new object[] {this};

            Game = (IPKGame)Activator.CreateInstance(type, args);
            Game.Initialize();
        }

        protected virtual void Start()
        {
            Game.Start();
        }

        protected virtual Type GetGameType()
        {
            throw new Exception("You dhould set custom Game type!");
        }

    #region Coroutines Interface
        public void StartGameCoroutine(IEnumerator routine)
        {
            StartCoroutine(routine);
        }

        public void StopGameCoroutine(IEnumerator routine)
        {
            StopCoroutine(routine);
        }

        public void StopAllGameCoroutines()
        {
            StopAllCoroutines();
        }
    #endregion
    }
}
