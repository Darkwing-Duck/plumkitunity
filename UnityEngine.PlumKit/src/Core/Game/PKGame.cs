﻿#region Using Statements
using System;
using System.Collections;
using UnityEngine;
#endregion

namespace PlumKit
{
    abstract public class PKGame : IPKGame
    {
        #region Members
        protected IPKSceneManager _sceneManager;
        protected PKGameControllerComponent _gameController;
        protected IPKScene _loadingScene;
        #endregion

        #region Constructor
        public PKGame(PKGameControllerComponent gameController)
        {
            _gameController = gameController;

            IsStarted = false;
            IsPaused = false;
        }
        #endregion

        #region Protected Methods
        protected virtual IPKLogger CreateLogger()
        {
            return new PKDefaultLogger();
        }
        
        protected virtual PKGameModel CreateModel()
        {
            return new PKGameModel(this);
        }
        
        protected virtual IPKSceneManager CreateSceneManager()
        {
            return new PKSceneManager(this);
        }
        
        protected virtual IPKScene CreateLoadingScene()
        {
            return new PKLoadingScene(this, _sceneManager);
        }
        
        protected virtual void RegisterScenes()
        {
            RegisterScene(_loadingScene);
        }
        
        protected void RegisterScene(IPKScene sceneController)
        {
            _sceneManager.RegisterScene(sceneController);
        }
        #endregion
        
        #region Game Handlers
        protected virtual void OnInitialize()
        {
        }
        
        protected virtual void OnStart()
        {
        }
        
        protected virtual void OnPauseEnter()
        {
        }
        
        protected virtual void OnPauseExit()
        {
        }
        
        protected virtual void OnUpdate()
        {
        }
        #endregion

        #region Public Methods
        public void Initialize()
        {
            Logger = CreateLogger();
            Model = CreateModel();

            _sceneManager = CreateSceneManager();
            _loadingScene = CreateLoadingScene();
            
            RegisterScenes();
            OnInitialize();
        }
        
        public void Start()
        {
            IsStarted = true;
            Logger.LogInfo("Game is started!", PKLoggerGroups.PLUM_KIT);
            
            _sceneManager.Init(_loadingScene.Alias);
            OnStart();
        }
        
        public void Pause()
        {
            IsPaused = true;
            Logger.LogInfo("Game is paused!", PKLoggerGroups.PLUM_KIT);
            OnPauseEnter();
        }
        
        public void Unpause()
        {
            IsPaused = false;
            Logger.LogInfo("Game is unpaused!", PKLoggerGroups.PLUM_KIT);
            OnPauseExit();
        }
        
        public void Update()
        {
            if (!IsStarted)
            {
                return;
            }
            
            if (IsPaused)
            {
                return;
            }
            
            _sceneManager.ActiveScene.Update();
            OnUpdate();
        }
        
        public void GoToScene(string sceneName)
        {
            _sceneManager.GoToScene(sceneName);
        }
        
        public void StartGameCoroutine(IEnumerator routine)
        {
            _gameController.StartGameCoroutine(routine);
        }
        
        public void StopGameCoroutine(IEnumerator routine)
        {
            _gameController.StopCoroutine(routine);
        }
        
        public void StopAllGameCoroutines()
        {
            _gameController.StopAllCoroutines();
        }
        
        public void Dispose()
        {
            IsStarted = false;
            IsPaused = false;
            
            Model.Dispose();
            _sceneManager.Dispose();
            
            Model = null;
            _sceneManager = null;
            _loadingScene = null;
        }
        #endregion

        #region Accessors
        public bool IsStarted { get; protected set; }
        
        public bool IsPaused { get; protected set; }
        
        public PKGameModel Model { get; protected set; }
        
        public IPKLogger Logger { get; protected set; }

        public IPKScene ActiveScene
        {
            get
            {
                return _sceneManager.ActiveScene;
            }
        }
        #endregion
    }
}