﻿#region Using Statements
using System;
using System.Collections;
#endregion

namespace PlumKit
{
    public interface IPKGame : IUpdatable, IDisposable
    {
        #region Public Methods
        void Initialize();
        void Start();
        void Pause();
        void Unpause();
        void GoToScene(string sceneName);
        void StartGameCoroutine(IEnumerator routine);
        void StopGameCoroutine(IEnumerator routine);
        void StopAllGameCoroutines();
        #endregion
        
        #region Accessors
        IPKLogger Logger { get; }
        PKGameModel Model { get; }
        IPKScene ActiveScene { get; } 
        #endregion
    }
}