#region Using Statements
using System;
#endregion

namespace PlumKit
{
    public class PKEmptyLogger : IPKLogger
    {
        public PKEmptyLogger()
        {
        }
        
        #region IPKLogger Methods
        public virtual void Log(object message)
        {
        }
        
        public virtual void Log(object message, Enum groupId)
        {
        }
        
        public virtual void LogInfo(object message)
        {
        }
        
        public virtual void LogInfo(object message, Enum groupId)
        {
        }
        
        public virtual void LogDebug(object message)
        {
        }
        
        public virtual void LogDebug(object message, Enum groupId)
        {
        }
        
        public virtual void LogWarning(object message)
        {
        }
        
        public virtual void LogWarning(object message, Enum groupId)
        {
        }
        
        public virtual void LogError(object message)
        {
        }
        
        public virtual void LogError(object message, Enum groupId)
        {
        }
        
        public virtual void LogCommand(SlashCommand command)
        {
        }
        
        public virtual void RegisterGroup(Enum groupId, string groupName)
        {
        }
        #endregion
        
        #region IPKSlashCommandManager Methods
        public virtual void RegisterCommand(SlashCommand command)
        {
        }
        
        public virtual SlashCommand UnregisterCommand(string name)
        {
            return null;
        }
        
        public virtual void ExecuteCommand(string commandLineValue)
        {
        }
        #endregion
        
        #region IDisposable Methods
        public virtual void Dispose()
        {
        }
        #endregion
    }
}