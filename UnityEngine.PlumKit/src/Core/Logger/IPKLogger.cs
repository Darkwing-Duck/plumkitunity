#region Using Statements
using System;
#endregion

namespace PlumKit
{
    public interface IPKLogger : IPKSlashCommandManager, IDisposable
    {
        #region Public Methods
        void Log(object message);
        void Log(object message, Enum groupId);
        
        void LogInfo(object message);
        void LogInfo(object message, Enum groupId);
        
        void LogDebug(object message);
        void LogDebug(object message, Enum groupId);
        
        void LogWarning(object message);
        void LogWarning(object message, Enum groupId);
        
        void LogError(object message);
        void LogError(object message, Enum groupId);
        
        void LogCommand(SlashCommand command);
        
        void RegisterGroup(Enum groupId, string groupName);
        #endregion
    }
}