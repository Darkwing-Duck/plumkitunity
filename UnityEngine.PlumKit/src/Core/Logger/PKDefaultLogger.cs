#region Using Statements
using System;
using System.Collections.Generic;
using UnityEngine;
#endregion

namespace PlumKit
{
    internal enum PKLoggerGroups
    {
        PLUM_KIT
    }

    public class PKDefaultLogger : PKEmptyLogger
    {
        protected Dictionary<Enum, string> _groups;
        protected IPKSlashCommandManager _commandManager;
        
        public PKDefaultLogger()
        {
            _groups = new Dictionary<Enum, string>();
            _commandManager = CreateCommandManager();
            
            RegisterInternalGroups();
        }
        
        private void RegisterInternalGroups()
        {
            RegisterGroup(PKLoggerGroups.PLUM_KIT, PKLoggerGroups.PLUM_KIT.ToString());
        }
        
        protected virtual IPKSlashCommandManager CreateCommandManager()
        {
            return new PKSlashCommandManager(this);
        }
        
        protected virtual void RegisterCustomGroups()
        {
        }
        
        protected string GetGroupPrefix(Enum groupId)
        {
            if (!_groups.ContainsKey(groupId))
            {
                LogWarning("The group id '" + groupId + "' was not registered!");
                return "";
            }
            
            return "[" + _groups[groupId] + "]: ";
        }
        
        #region IPKLogger Methods
        public override void Log(object message)
        {
            Debug.Log("<color=#E1FF63FF>" + message + "</color>");
        }
        
        public override void Log(object message, Enum groupId)
        {
            Debug.Log("<color=#E1FF63FF>" + GetGroupPrefix(groupId) + message + "</color>");
        }
        
        public override void LogInfo(object message)
        {
            Debug.Log("<color=#33CCFFFF>" + message + "</color>");
        }
        
        public override void LogInfo(object message, Enum groupId)
        {
            Debug.Log("<color=#33CCFFFF>" + GetGroupPrefix(groupId) + message + "</color>");
        }
        
        public override void LogDebug(object message)
        {
            Debug.Log("<color=#FFFFFFFF>" + message + "</color>");
        }
        
        public override void LogDebug(object message, Enum groupId)
        {
            Debug.Log("<color=#FFFFFFFF>" + GetGroupPrefix(groupId) + message + "</color>");
        }
        
        public override void LogCommand(SlashCommand command)
        {
            Debug.Log("<color=#C685FFFF>" + command.ToString() + "</color>");
        }
        
        public override void LogWarning(object message)
        {
            Debug.LogWarning("<color=#FFCC00FF>" + message + "</color>");
        }
        
        public override void LogWarning(object message, Enum groupId)
        {
            Debug.LogWarning("<color=#FFCC00FF>" + GetGroupPrefix(groupId) + message + "</color>");
        }

        public override void LogError(object message)
        {
            Debug.LogError("<color=#FF3333FF>" + message + "</color>");
        }
        
        public override void LogError(object message, Enum groupId)
        {
            Debug.LogError("<color=#FF3333FF>" + GetGroupPrefix(groupId) + message + "</color>");
        }
        
        public override void RegisterGroup(Enum groupId, string groupName)
        {
            _groups.Add(groupId, groupName);
        }
        #endregion
        
        #region IPKSlashCommandManager Methods
        public override void RegisterCommand(SlashCommand command)
        {
            _commandManager.RegisterCommand(command);
        }
        
        public override SlashCommand UnregisterCommand(string name)
        {
            return _commandManager.UnregisterCommand(name);
        }
        
        public override void ExecuteCommand(string commandLineValue)
        {
            _commandManager.ExecuteCommand(commandLineValue);
        }
        #endregion
        
        #region IDisposable Methods
        public override void Dispose()
        {
            _commandManager.Dispose();
            _commandManager = null;
            _groups = null;
        }
        #endregion
    }
}