using System;
using System.Collections.Generic;

namespace PlumKit
{
    public class PKSlashCommandManager : IPKSlashCommandManager
    {
        protected IPKLogger _logger;
        protected Dictionary<string, SlashCommand> _commands;
        
        public PKSlashCommandManager(IPKLogger logger)
        {
            _logger = logger;
            _commands = new Dictionary<string, SlashCommand>();
        }
        
        public void RegisterCommand(SlashCommand command)
        {
            if (_commands.ContainsKey(command.Name))
            {
                throw new Exception("The command '" + command.Name + "' is already registered!");
            }
            
            _commands.Add(command.Name, command);
        }
        
        public SlashCommand UnregisterCommand(string name)
        {
            if (!_commands.ContainsKey(name))
            {
                throw new Exception("The command '" + name + "' was not registered!");
            }
            
            SlashCommand result = _commands[name];
            _commands.Remove(name);
            return result;
        }
        
        public void ExecuteCommand(string commandLineValue)
        {
            string[] splitArr = commandLineValue.Split(' ');
            List<string> splitList = new List<string>(splitArr);
            
            // get command name
            string commandName = splitList[0];
            commandName = commandName.Substring(1);
            splitList.RemoveAt(0);
            //
 
            if (!_commands.ContainsKey(commandName))
            {
                _logger.LogWarning("The command '" + commandName + "' was not registered!", PKLoggerGroups.PLUM_KIT);
                return;
            }
            
            string[] args = splitList.ToArray();
            
            SlashCommand command = _commands[commandName];
            command.Args = args;
            
            _logger.LogCommand(command);
            command.Execute();
        }

        public void Dispose()
        {
            _commands = null;
            _logger = null;
        }
    }
}