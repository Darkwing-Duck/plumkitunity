using System;

namespace PlumKit
{
    public delegate void SlashCommandCallback(SlashCommand command);

    public class SlashCommand
    {
        public const string SLASH_COMMAND_PREFIX = "/";
        public const string SLASH_COMMAND_INDICATOR = "->";
        public const string SLASH_COMMAND_RESULT_INDICATOR = "<-";
    
        protected string _name;
        protected string _description;
        protected string[] _args;

        public SlashCommandCallback Callback { get; set; }
        
        public SlashCommand(string name, string description)
        {
            _name = name;
            _description = description;
        }
        
        public void Execute()
        {
            if (Callback != null)
            {
                Callback(this);
            }
        }
        
        public override string ToString()
        {
            string result = SLASH_COMMAND_INDICATOR + " " + _name;
            string args = "(";

            if (_args.Length > 0)
            {
                foreach (string arg in _args)
                {
                    args += arg + ",";
                }
                
                args = args.Substring(0, args.Length - 1);
            }
            
            args += ")";
            
            return result + args;
        }
        
        protected string GenerateResult()
        {
            return "";
        }
        
        public string Result()
        {
            string result = GenerateResult();
            
            if (result.Length > 0)
            {
                result = SLASH_COMMAND_RESULT_INDICATOR + " " + result;
            }
            
            return result;
        }
        
        public void Dispose()
        {
            Callback = null;
        }
        
        public string Name 
        { 
            get { return _name; }
        }
        
        public string Description 
        { 
            get { return _description; }
        }
        
        public string[] Args 
        { 
            get { return _args; }
            set { _args = value; }
        }
    }
}

