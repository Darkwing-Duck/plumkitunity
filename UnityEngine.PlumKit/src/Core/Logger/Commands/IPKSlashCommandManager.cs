using System;

namespace PlumKit
{
    public interface IPKSlashCommandManager : IDisposable
    {
        void RegisterCommand(SlashCommand command);
        SlashCommand UnregisterCommand(string name);
        void ExecuteCommand(string commandLineValue);
    }
}

