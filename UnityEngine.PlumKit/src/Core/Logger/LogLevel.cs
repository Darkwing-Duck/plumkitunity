using System;

namespace PlumKit
{
    public enum LogLevel
    {
        LOG,
        INFO,
        DEBUG,
        WARNING,
        ERROR,
        COMMAND
    }
}

