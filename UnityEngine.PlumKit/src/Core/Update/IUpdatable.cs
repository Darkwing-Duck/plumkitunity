using System;

namespace PlumKit
{
    public interface IUpdatable
    {
        #region Public Methods
        void Update();
        #endregion

        #region Accessors
        #endregion
    }
}