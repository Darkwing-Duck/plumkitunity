﻿using System;

namespace PlumKit
{
    public interface IPKSceneManager : IDisposable
    {
        #region Public Methods
        void Init(string sceneName);
        void GoToScene(string sceneName);
        void RegisterScene(IPKScene sceneController);
        #endregion
        
        #region Accessors
        IPKScene ActiveScene { get; }
        bool IsSceneRegistered(string sceneName);
        #endregion
    }
}