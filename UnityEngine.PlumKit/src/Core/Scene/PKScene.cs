﻿#region Using Statements
using System;
using System.Collections;
using UnityEngine;
#endregion

namespace PlumKit
{
    abstract public class PKScene : IPKScene
    {
        #region Members
        protected PKSceneModel _model;
        protected IPKGame _game;
        protected IPKSceneManager _sceneManager;
        protected AsyncOperation _asyncOperation;
        #endregion

        #region Constructor
        public PKScene(IPKGame game, IPKSceneManager sceneManager)
        {
            _game = game;
            _sceneManager = sceneManager;
        }
        #endregion

        #region Scene Handlers
        protected virtual void OnInitialize()
        {
        }
        
        protected virtual void OnStartLoading()
        {
        }

        protected virtual void OnLoadingProgress(float progress)
        {
        }

        protected virtual void OnActivate()
        {
        }

        protected virtual void OnDeactivate()
        {
        }
        #endregion

        #region Protected Methods
        protected virtual IEnumerator ProcessLoadingScene()
        {
            _asyncOperation = Application.LoadLevelAsync(Alias);
            
            while (!_asyncOperation.isDone)
            {
                OnLoadingProgress(_asyncOperation.progress);
                yield return null;
            }
            
            _asyncOperation = null;
            Activate();
        }
        
        protected virtual PKSceneModel CreateModel()
        {
            return new PKSceneModel(_game);
        }
        #endregion
        
        #region Internal Methods
        internal void Initialize()
        {
            _model = CreateModel();
            _game.Logger.LogInfo("Scene " + Alias + " is initialized!", PKLoggerGroups.PLUM_KIT);
            
            OnInitialize();
        }
        
        internal void StartLoading()
        {
            _game.Logger.LogInfo("Scene " + Alias + " start loading!", PKLoggerGroups.PLUM_KIT);
            OnStartLoading();
            _game.StartGameCoroutine(ProcessLoadingScene());
        }
        
        internal void Activate()
        {
            Resources.UnloadUnusedAssets();
            _game.Logger.LogInfo("Scene " + Alias + " is activated!", PKLoggerGroups.PLUM_KIT);
            OnActivate();
        }
        
        internal void Deactivate()
        {
            _game.Logger.LogInfo("Scene " + Alias + " is deactivated!", PKLoggerGroups.PLUM_KIT);
            OnDeactivate();
        }
        #endregion

        #region Public Methods
        public virtual void Update()
        {
        }
        
        public void Dispose()
        {
            _model.Dispose();
            
            _model = null;
            _game = null;
            _sceneManager = null;
        }
        #endregion

        #region Accessors
        public PKSceneModel Model
        {
            get
            {
                return _model;
            }
        }

        public bool IsActive
        {
            get
            {
                return _sceneManager.ActiveScene == this;
            }
        }

        abstract public string Alias { get; }
        #endregion
    }
}