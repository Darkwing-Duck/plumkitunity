﻿using System;
using System.Collections;

namespace PlumKit
{
    public interface IPKScene : IUpdatable, IDisposable
    {
        #region Public Methods
        #endregion
        
        #region Accessors
        PKSceneModel Model { get; }
        bool IsActive { get; }
        string Alias { get; }
        #endregion
    }
}