﻿using System;
using System.Collections.Generic;

namespace PlumKit
{
    public class PKSceneManager : IPKSceneManager
    {
        protected IPKScene _activeScene;
        protected IPKGame _game;
        protected Dictionary<string, IPKScene> _scenes;

        public PKSceneManager(IPKGame game)
        {
            _game = game;
            _scenes = new Dictionary<string, IPKScene>();
        }

        public void RegisterScene(IPKScene sceneController)
        {
            if (IsSceneRegistered(sceneController.Alias))
            {
                throw new Exception("The scene '" + sceneController.Alias + "' already registered!");
            }

            _scenes.Add(sceneController.Alias, sceneController);
        }

        public bool IsSceneRegistered(string sceneName)
        {
            return _scenes.ContainsKey(sceneName);
        }

        public void Init(string sceneName)
        {
            _activeScene = _scenes [sceneName];
            (_activeScene as PKScene).Initialize();
            (_activeScene as PKScene).Activate();
        }

        public void GoToScene(string sceneName)
        {
            if (!IsSceneRegistered(sceneName))
            {
                throw new Exception("The scene '" + sceneName + "' was not registered!");
            }

            DeactivateActiveScene();

            _activeScene = _scenes [sceneName];
            (_activeScene as PKScene).Initialize();
            (_activeScene as PKScene).StartLoading();
        }

        public IPKScene ActiveScene
        {
            get
            {
                return _activeScene;
            }
        }

        protected virtual void DeactivateActiveScene()
        {
            if (_activeScene != null)
            {
                (_activeScene as PKScene).Deactivate();
                _activeScene = null;
            }
        }

        private void DisposeAllScenes()
        {
            DeactivateActiveScene();

            foreach (KeyValuePair<string, IPKScene> pair in _scenes)
            {
                pair.Value.Dispose();
            }

            _scenes.Clear();
        }

        public void Dispose()
        {
            _game = null;
            _scenes = null;
        }
    }
}