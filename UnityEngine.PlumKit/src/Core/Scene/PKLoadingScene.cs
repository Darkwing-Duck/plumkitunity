﻿using System;

namespace PlumKit
{
    public class PKLoadingScene : PKScene
    {
        public PKLoadingScene(IPKGame game, IPKSceneManager sceneManager) : base(game, sceneManager)
        {
        }

        public override string Alias
        {
            get
            {
                return "PKLoadingScene";
            }
        }
    }
}