﻿using System;

namespace PlumKit
{
    abstract public class PKModel : IPKModel
    {
        protected IPKGame _game;

        public PKModel(IPKGame game)
        {
            _game = game;
        }

        public void Dispose()
        {
            _game = null;
        }
    }
}